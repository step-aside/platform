/**
 * $Revision$
 * $Date$
 *
 * Copyright (C) https://gitee.com/baibaiclouds/platform loon. All rights reserved.
 * <p>
 * This software is the confidential and proprietary information of loon.
 * You shall not disclose such Confidential Information and shall use it only
 * in accordance with the terms of the agreements you entered into with loon.
 * 
 * Modified history:
 *   Administrator  2019年7月12日 下午11:49:37  created
 */
package com.desktop.web.core.utils;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.alibaba.fastjson.JSON;
import com.desktop.web.core.exception.BusinessException;
import com.desktop.web.service.websession.WebSession;
import com.desktop.web.uda.entity.User;

/**
 * 
 *
 * @author baibai
 */
public class RequestUtil {

    private static Logger logger = LoggerFactory.getLogger(RequestUtil.class);

    /**
     * 获取用户真实IP地址
     * 
     * @param request
     * @return
     */
    public static String getIP(HttpServletRequest request) {
        String ip = request.getHeader("x-forwarded-for");
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("WL-Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("HTTP_CLIENT_IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("HTTP_X_FORWARDED_FOR");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getRemoteAddr();
        }
        return ip;
    }

    /**
     * 获取授权的用户登录名称
     * 
     * @return
     */
    public static String getUsername() {
        HttpServletRequest request = ((ServletRequestAttributes) (RequestContextHolder.currentRequestAttributes())).getRequest();
        Object obj = request.getAttribute("webSession");
        if (obj == null) {
            throw new BusinessException("未授权");
        }

        WebSession webSession = (WebSession) obj;
        return webSession.getUsername();
    }

    /**
     * 获取授权的用户id
     * 
     * @return
     */
    public static Long getUid() {
        HttpServletRequest request = ((ServletRequestAttributes) (RequestContextHolder.currentRequestAttributes())).getRequest();
        Object obj = request.getAttribute("webSession");
        if (obj == null) {
            throw new BusinessException("未授权");
        }

        WebSession webSession = (WebSession) obj;
        return webSession.getUid();
    }

    /**
     * 获取授权的用户信息
     * 
     * @return
     */
    public static User getUser() {
        HttpServletRequest request = ((ServletRequestAttributes) (RequestContextHolder.currentRequestAttributes())).getRequest();
        Object obj = request.getAttribute("webSession");
        if (obj == null) {
            throw new BusinessException("未授权");
        }

        WebSession webSession = (WebSession) obj;
        User user = new User();
        user.setId(webSession.getUid());
        user.setUsername(webSession.getUsername());
        return user;
    }

    /**
     * 获取body json数据
     * 
     * @param request
     * @return
     */
    @SuppressWarnings("unchecked")
    public static Map<String, String> getBody(HttpServletRequest request) {
        try {
            BufferedReader streamReader = new BufferedReader(new InputStreamReader(request.getInputStream(), "UTF-8"));
            StringBuilder responseStrBuilder = new StringBuilder();
            String inputStr;
            while ((inputStr = streamReader.readLine()) != null) {
                responseStrBuilder.append(inputStr);
            }
            return JSON.parseObject(responseStrBuilder.toString(), Map.class);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }

        return null;
    }

    @SuppressWarnings("unchecked")
    public static Map<String, Object> getBody2(HttpServletRequest request) {
        try {
            BufferedReader streamReader = new BufferedReader(new InputStreamReader(request.getInputStream(), "UTF-8"));
            StringBuilder responseStrBuilder = new StringBuilder();
            String inputStr;
            while ((inputStr = streamReader.readLine()) != null) {
                responseStrBuilder.append(inputStr);
            }
            return JSON.parseObject(responseStrBuilder.toString(), Map.class);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }

        return null;
    }
}
