/**
 * $Revision$
 * $Date$
 *
 * Copyright (C) https://gitee.com/baibaiclouds/platform . All rights reserved.
 * <p>
 * This software is the confidential and proprietary information of .
 * You shall not disclose such Confidential Information and shall use it only
 * in accordance with the terms of the agreements you entered into with .
 * 
 * Modified history:
 *   baibai  2021年4月18日 下午5:54:40  created
 */
package com.desktop.web.service.config;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.desktop.web.core.exception.BusinessException;
import com.desktop.web.uda.entity.Config;
import com.desktop.web.uda.mapper.ConfigMapper;

/**
 * 
 *
 * @author baibai
 */
@Service
public class ConfigService {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private ConfigMapper configMapper;

    private String systemUuid = null;
    private Long adminRootUid = null;
    private Long rootNodeId = null;
    private Long unknowNodeId = null;

    /**
     * 获取配置参数
     * 
     * @param title
     * @return
     */
    public String getValue(String title) {

        Config entity = new Config();
        entity.setTitle(title);
        QueryWrapper<Config> queryWrapper = new QueryWrapper<Config>();
        queryWrapper.setEntity(entity);
        entity = configMapper.selectOne(queryWrapper);
        if (entity == null) {
            return null;
        }

        return entity.getValue();
    }

    /**
     * 设置配置信息
     * 
     * @param title
     * @param value
     */
    public void setValue(String title, Object value) {

        if (StringUtils.isEmpty(title)) {
            return;
        }

        Config entity = new Config();
        entity.setTitle(title);
        QueryWrapper<Config> queryWrapper = new QueryWrapper<Config>();
        queryWrapper.setEntity(entity);
        entity = configMapper.selectOne(queryWrapper);
        if (entity == null) {
            return;
        }

        Config update = new Config();
        update.setId(entity.getId());
        update.setValue(value.toString());
        configMapper.updateById(update);

        logger.info("update config success,title:{},value:{}", title, value.toString());
    }

    /**
     * 添加配置项
     * 
     * @param title
     * @param value
     */
    public void addValue(String title, Object value) {
        Config add = new Config();
        add.setTitle(title);
        add.setValue(value.toString());
        configMapper.insert(add);
    }

    /**
     * 获取安装天数
     * 
     * @return
     */
    public String getInitTimeDays() {
        Object tempval = this.getValue("init_time");
        if (tempval == null) {
            return "0";
        }

        Date cur = new Date();
        Date date = new Date(Long.valueOf(tempval.toString()));
        return String.valueOf((cur.getTime() - date.getTime()) / 1000 / 86400);
    }

    /**
     * 获取系统ID
     * 
     * @return
     */
    public String getSystemuuid() {

        if (!StringUtils.isEmpty(systemUuid)) {
            return systemUuid;
        }

        String uuid = this.getValue("system_uuid");
        if (StringUtils.isEmpty(uuid)) {
            throw new BusinessException("产品ID获取失败，请检测部署是否正常");
        }

        systemUuid = uuid;

        return uuid;
    }

    /**
     * 获取初始超级管理员UID
     * 
     * @return
     */
    public Long getAdminRootUID() {

        if (adminRootUid != null) {
            return adminRootUid;
        }

        String value = this.getValue("admin_root_uid");
        adminRootUid = Long.valueOf(value);

        return adminRootUid;
    }

    /**
     * 获取顶级设备节点id
     * 
     * @return
     */
    public Long getRootNodeId() {

        if (rootNodeId != null) {
            return rootNodeId;
        }

        String value = this.getValue("root_node_id");
        rootNodeId = Long.valueOf(value);
        return rootNodeId;
    }

    /**
     * 获取团队名称
     * 
     * @return
     */
    public String getTeamName() {
        String name = this.getValue("team_name");
        if (name == null) {
            return "";
        }

        return name;
    }

    /**
     * 获取未知节点列表
     * 
     * @return
     */
    public Long getUnknowNodeId() {
        if (unknowNodeId != null) {
            return unknowNodeId;
        }

        String value = this.getValue("unknow_node_id");
        unknowNodeId = Long.valueOf(value);

        return unknowNodeId;
    }

}
